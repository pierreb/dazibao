package main

/* Ce fichier est un exemple de l’utilisation de l’implémentation du
protocole dazibao.
Il reste suffisamment manipulable pour par exemple afficher la liste des
nœeuds, ou même faire des bots similaires à ceux exposés précédemment. */

import (
	"flag"
	"fmt"
	"gitlab.crans.org/pierreb/dazibao/dazibao"
	"time"
)

func main() {
	/* init cli flags */
	debug := flag.Int("debug", 0, "Set debug level (0 (min) to 4 (max)) (default 0)")
	port := flag.Int("port", 0, "Specifies the node port (useful for testing)")
	msg := flag.String("msg", "Hello world", "Specifies a custom message to propagate")
	id := flag.String("id", "", "Specifies a custom ID")
	ineigh := flag.String("neigh", "jch.irif.fr:1212", "Specifies initial neighbour")
	flag.Parse()

	dazibao.Launch(*debug, *port, *msg, *id, *ineigh)

	deadline := time.Date(2019, time.December, 9, 3, 0, 0, 0, time.FixedZone("UTC+0", 1*60))

	for {
		u := time.Since(deadline)
		msg := fmt.Sprintf("C’est trop tard depuis %s... Désolé :(", u.String())
		dazibao.SetLocalMsg(msg)
		fmt.Println("Changed local msg")
		time.Sleep(60 * time.Second)
	}
}
