package main

import (
	"flag"
	"fmt"
	"gitlab.crans.org/pierreb/dazibao/dazibao"
	"html"
	"io"
	"net/http"
	"sort"
)

func hello(w http.ResponseWriter, r *http.Request) {
	u := dazibao.GetNetwork()
	v := []string{}
	for k, _ := range u {
		v = append(v, k)
	}

	sort.Strings(v)

	io.WriteString(w, "<!DOCTYPE html><html><head><title>Dazibao network state</title></head><body>\n")
	io.WriteString(w, "<table><tr><th>Node</th><th>Sequence number</th><th>Data</th></tr>")
	for _, k := range v {
		a := u[k]
		io.WriteString(w, fmt.Sprintf("<tr><td>%s</td><td>%d</td><td>%s</td></tr>\n",
			(&a).String(),
			a.SeqNo,
			html.EscapeString(string(a.Data))))
	}
	io.WriteString(w, "</table></body></html>\n")
}

func main() {
	/* init cli flags */
	debug := flag.Int("debug", 0, "Set debug level (0 (min) to 4 (max)) (default 0)")
	port := flag.Int("port", 0, "Specifies the node port (useful for testing)")
	msg := flag.String("msg", "Hello world", "Specifies a custom message to propagate")
	id := flag.String("id", "", "Specifies a custom ID")
	ineigh := flag.String("neigh", "zamok.crans.org:1212", "Specifies initial neighbour")
	flag.Parse()

	dazibao.Launch(*debug, *port, *msg, *id, *ineigh)

	http.HandleFunc("/", hello)
	http.ListenAndServe(":8082", nil)
}
