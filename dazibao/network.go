package dazibao

import (
	"crypto/sha256"
	"sort"
)

type Network map[string]Node

func (n Network) Hash() []byte {
	// To store the keys in slice in sorted order
	var keys []string
	for k := range n {
		keys = append(keys, k)
	}
	sort.Strings(keys)

	h := sha256.New()
	for _, k := range keys {
		h.Write(n[k].Hash())
	}
	return h.Sum(nil)
}
