package dazibao

import (
	"crypto/sha256"
	"encoding/binary"
	"encoding/hex"
	"math/rand"
	"time"
)

func seqLt(a, b uint16) bool {
	return ((b - a) & (1 << 15)) == 0
}

func newId(ret *[8]byte) {
	s1 := rand.NewSource(time.Now().UnixNano())
	r1 := rand.New(s1)
	r1.Read(ret[:])
}

const DataMaxLen = 192

type Node struct {
	Id    [8]byte
	SeqNo uint16
	Data  []byte
}

func (n Node) Hash() []byte {
	h := sha256.New()
	h.Write(n.Id[:])
	binary.Write(h, binary.BigEndian, n.SeqNo)
	h.Write(n.Data)
	return h.Sum(nil)
}

func (n *Node) SetMsg(s string) {
	a := []byte(s)
	if len(a) <= DataMaxLen {
		n.Data = a
	} else {
		n.Data = a[0:DataMaxLen]
	}
}

func (n *Node) IncSeq() {
	n.SeqNo++
}

func (n *Node) String() string {
	return hex.EncodeToString(n.Id[:])
}
