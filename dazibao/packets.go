package dazibao

import (
	"encoding/binary"
	"encoding/hex"
	"errors"
	"fmt"
	"net"
)

const (
	Pad1 uint8 = iota
	PadN
	NeigReq
	Neig
	NetHash
	NetStateReq
	NodeHash
	NodeStateReq
	NodeState
	Warning
)

type TLVType uint8

func (t TLVType) String() string {
	s := []string{"Pad1", "PadN", "Neighbour Request", "Neighbour",
		"Network Hash", "Network State Request", "Node Hash", "Node State Request",
		"Node State", "Warning"}[uint8(t)]
	return fmt.Sprintf("%s(%d)", s, uint8(t))
}

type TLV struct {
	Type   uint8
	Length uint8
	IP     net.IP
	Port   uint16
	Hash   []byte
	NodeID [8]byte
	SeqNo  uint16
	Data   []byte
}

func (tlv *TLV) Serialize() []byte {
	var b []byte

	switch tlv.Type {

	case Pad1:
		b = []byte{Pad1}

	case PadN:
		b = make([]byte, tlv.Length+2)
		copy(b[0:2], []byte{PadN, tlv.Length})

	case NeigReq:
		b = []byte{NeigReq, 0}

	case Neig:
		b = make([]byte, 20)
		copy(b[0:2], []byte{Neig, 18})
		copy(b[2:18], tlv.IP.To16())
		binary.BigEndian.PutUint16(b[18:20], tlv.Port)

	case NetHash:
		b = make([]byte, 34)
		copy(b[0:2], []byte{NetHash, 32})
		copy(b[2:34], tlv.Hash)

	case NetStateReq:
		b = []byte{NetStateReq, 0}

	case NodeHash:
		b = make([]byte, 44)
		copy(b[0:2], []byte{NodeHash, 42})
		copy(b[2:10], tlv.NodeID[:])
		binary.BigEndian.PutUint16(b[10:12], tlv.SeqNo)
		copy(b[12:44], tlv.Hash)

	case NodeStateReq:
		b = make([]byte, 10)
		copy(b[0:2], []byte{NodeStateReq, 8})
		copy(b[2:10], tlv.NodeID[:])

	case NodeState:
		b = make([]byte, 44+len(tlv.Data))
		copy(b[0:2], []byte{NodeState, byte(42 + len(tlv.Data))})
		copy(b[2:10], tlv.NodeID[:])
		binary.BigEndian.PutUint16(b[10:12], tlv.SeqNo)
		copy(b[12:44], tlv.Hash)
		copy(b[44:], tlv.Data)

	case Warning:
		b = make([]byte, 2+len(tlv.Data))
		copy(b[0:2], []byte{Warning, byte(42 + len(tlv.Data))})
		copy(b[2:], tlv.Data)
	}

	return b
}

type packet []byte

func (p packet) Check() (err error) {
	plog.Println("checking")
	if len(p) < 4 {
		err = errors.New("packet too small")
		return
	}
	if p[0] != 95 {
		err = errors.New(fmt.Sprintf("wrong magic number: %d", p[0]))
		return
	}
	if p[1] != 0 {
		err = errors.New(fmt.Sprintf("wrong version number: %d", p[1]))
		return
	}
	if s := binary.BigEndian.Uint16(p[2:4]); s+4 > uint16(len(p)) {
		err = errors.New(fmt.Sprintf("packet too small : got %d, expected %d", len(p), s+4))
		return
	}

	return
}

func newPacket() packet {
	p := []byte{95, 0, 0, 0}
	return p
}

func (p *packet) AddTLV(tlv TLV) (err error) {
	b := tlv.Serialize()
	if len(*p)+len(b) > 1024 {
		err = errors.New("packet too long")
		return
	}
	*p = append(*p, b...)
	binary.BigEndian.PutUint16((*p)[2:4], uint16(len(*p))-4)
	return
}

func (p packet) Parse() (out []TLV, err error) {
	plog.Println("beginning")
	err = p.Check()
	if err != nil {
		return
	}

	packetLength := binary.BigEndian.Uint16(p[2:4])
	plog.Printf("packet length %d vs size %d", packetLength, len(p))
	b := p[4 : packetLength+4]

	/* On lit un TLV par tour de boucle */
	for len(b) > 0 {

		/* cas d’un tlv de padding */
		if b[0] == Pad1 {
			plog.Printf("tlv %s", TLVType(b[0]).String())
			b = b[1:]
			continue
		}

		if b[0] > 9 {
			err = errors.New(fmt.Sprintf("wrong TLV type %d at byte %x", b[0], len(p)-len(b)))
			return
		}

		/* taille minimum de chaque tlv : on ne fait pas confiance au nœud distant
		pour la taille des paquets */
		minSize := []byte{1, 2, 2, 20, 34, 2, 44, 10, 44, 2}

		if len(b) < int(minSize[b[0]]) {
			err = errors.New("tlv too short")
			return
		}

		tlv := new(TLV)
		tlv.Type = b[0]

		plog.Printf("tlv %s", TLVType(b[0]).String())

		switch b[0] {
		case PadN:
			length := b[1]
			b = b[length+2:]
			plog.Printf("skipped %d zeros", length)
			continue

		case NeigReq:
		case Neig:
			tlv.IP = make([]byte, 16)
			copy(tlv.IP, b[2:18])
			tlv.Port = binary.BigEndian.Uint16(b[18:20])
			plog.Printf("neighbour: %s:%d", tlv.IP.String(), tlv.Port)

		case NetHash:
			tlv.Hash = make([]byte, 32)
			copy(tlv.Hash, b[2:34])
			plog.Printf("hash: %s", hex.EncodeToString(tlv.Hash))

		case NetStateReq:
		case NodeHash:
			copy(tlv.NodeID[:], b[2:10])
			tlv.SeqNo = binary.BigEndian.Uint16(b[10:12])
			tlv.Hash = make([]byte, 32)
			copy(tlv.Hash, b[12:44])
			plog.Printf("node: %s, seqno: %d, hash: %s",
				hex.EncodeToString(tlv.NodeID[:]),
				tlv.SeqNo,
				hex.EncodeToString(tlv.Hash))

		case NodeStateReq:
			copy(tlv.NodeID[:], b[2:10])
			plog.Printf("node: %s", hex.EncodeToString(tlv.NodeID[:]))

		case NodeState:
			length := b[1]
			if length+2 < minSize[b[0]] || len(b) < int(length+2) {
				plog.Printf("tlv too short: declared %d bytes", length)
				err = errors.New("tlv too short")
				return
			}
			if length+2 > minSize[b[0]]+192 {
				plog.Printf("data too long: declared %d bytes", length)
				err = errors.New("data too long")
				return
			}
			tlv.Hash = make([]byte, 32)
			tlv.Data = make([]byte, length+2-minSize[b[0]])
			copy(tlv.NodeID[:], b[2:10])
			tlv.SeqNo = binary.BigEndian.Uint16(b[10:12])
			copy(tlv.Hash, b[12:44])
			copy(tlv.Data, b[44:length+2])
			plog.Printf("node: %s, seqno: %d, hash: %s, data: %s",
				hex.EncodeToString(tlv.NodeID[:]),
				tlv.SeqNo,
				hex.EncodeToString(tlv.Hash),
				tlv.Data)

			b = b[length+2-minSize[b[0]]:]

		case Warning:
			length := b[1]
			if len(b) < int(length+2) {
				err = errors.New("tlv too short")
			}
			tlv.Data = make([]byte, length)
			copy(tlv.Data, b[2:length+2])
			plog.Printf("warning: %s", tlv.Data)
			b = b[length:]

		default:
			/* should never be executed */
			err = errors.New("got invalid tlv type")
			return
		}

		out = append(out, *tlv)
		b = b[minSize[tlv.Type]:]
	}

	plog.Println("finished")

	return
}
