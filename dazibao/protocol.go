package dazibao

import (
	"bytes"
	"encoding/hex"
	//	"fmt"
	"io/ioutil"
	"log"
	"net"
	"os"
	"sync"
	"time"
)

/* different log levels */
var wlog *log.Logger
var ilog *log.Logger
var elog *log.Logger
var plog *log.Logger

type Neighbour struct {
	Addr      net.UDPAddr
	Permanent bool
	LastSeen  time.Time
}

func GetNeighbours() map[string]Neighbour {
	ret := make(map[string]Neighbour)
	neighlock.Lock()
	for k, v := range neighbours {
		ret[k] = v
	}
	neighlock.Unlock()
	return ret
}

func GetNetwork() Network {
	ret := make(Network)
	netlock.Lock()
	for k, v := range network {
		ret[k] = v
	}
	netlock.Unlock()
	return ret
}

func SetLocalMsg(s string) {
	neighlock.Lock()
	localLock.Lock()
	netlock.Lock()

	localNode.SetMsg(s)
	localNode.IncSeq()
	addToNet(localNode)

	/* on publie immédiatement la modification */
	tlv := TLV{Type: NodeState, NodeID: localNode.Id, SeqNo: localNode.SeqNo, Hash: localNode.Hash(),
		Data: localNode.Data}
	for _, v := range neighbours {
		addToQueue(v.Addr, tlv, write_chan)
		ilog.Printf("TLV to %s : Network Hash", v.Addr.String())
	}

	netlock.Unlock()
	localLock.Unlock()
	neighlock.Unlock()
}

func SetLocalId(id string) error {
	localLock.Lock()
	netlock.Lock()

	u, err := hex.DecodeString(id)
	if err != nil {
		return err
	} else {
		copy(localNode.Id[:], u)
	}
	localNode.IncSeq()
	addToNet(localNode)

	netlock.Lock()
	localLock.Unlock()
	return nil
}

/* Global data */
var localNode Node
var network Network
var neighbours map[string]Neighbour

type pack struct {
	Tlv  TLV
	Addr net.UDPAddr
}

var write_chan chan pack

var netlock sync.Mutex
var neighlock sync.Mutex
var localLock sync.Mutex

func updateNeighbour(a string, p bool) {
	addr, err := net.ResolveUDPAddr("udp", a)
	if err != nil {
		elog.Fatal(err)
	}
	neighlock.Lock()
	i, e := neighbours[addr.String()]
	if !e {
		neighbours[addr.String()] =
			Neighbour{Addr: *addr, Permanent: p, LastSeen: time.Now()}
		if !p {
			ilog.Printf("Added temporary neighbour %s", addr.String())
		} else {
			ilog.Printf("Added permanent neighbour %s", addr.String())
		}
	} else {
		i.LastSeen = time.Now()
		neighbours[addr.String()] = i
	}
	neighlock.Unlock()
}

func addToNet(n Node) {
	network[n.String()] = n
}

func getRandNeighbour() (n Neighbour) {
	for _, i := range neighbours {
		n = i
		return
	}
	elog.Fatal("No neighbour :-(")
	return
}

func addToQueue(addr net.UDPAddr, tlv TLV, c chan pack) {
	p := pack{Tlv: tlv, Addr: addr}
	c <- p
}

func Launch(debug, port int, msg, id, ineigh string) {
	done := make(chan bool)
	go launch(debug, port, msg, id, ineigh, done)
	<-done
	close(done)
}

func launch(debug, port int, msg, id, ineigh string, done chan bool) {
	network = make(Network)
	neighbours = make(map[string]Neighbour)
	write_chan = make(chan pack, 4096)

	/* init logging */
	logFlags := log.Ltime | log.Lshortfile
	wlog = log.New(os.Stderr, "[W] ", logFlags)
	ilog = log.New(os.Stderr, "[I] ", logFlags)
	elog = log.New(os.Stderr, "[E] ", logFlags)
	plog = log.New(os.Stderr, "[P] ", logFlags)

	/* init cli flags */
	switch debug {
	case 0:
		elog.SetOutput(ioutil.Discard)
		fallthrough
	case 1:
		wlog.SetOutput(ioutil.Discard)
		fallthrough
	case 2:
		ilog.SetOutput(ioutil.Discard)
		fallthrough
	case 3:
		plog.SetOutput(ioutil.Discard)
	default:
	}

	/* Hardcoded initial neighbours */
	updateNeighbour(ineigh, true)

	/* Init node */
	if id == "" {
		newId(&localNode.Id)
	} else {
		u, err := hex.DecodeString(id)
		if err != nil {
			wlog.Printf("%s. choosing a random id", err)
			newId(&localNode.Id)
		} else {
			copy(localNode.Id[:], u)
		}
	}
	localNode.SetMsg(msg)
	ilog.Printf("Initialised node id to %s with %s", localNode.String(), msg)
	addToNet(localNode)

	/* Init network */
	laddr := new(net.UDPAddr)
	laddr.Port = port
	conn, err := net.ListenUDP("udp", laddr)
	if err != nil {
		elog.Fatal(err)
	}
	ilog.Printf("Initialised node at %s", conn.LocalAddr().String())
	defer conn.Close()

	/* finished initialisation */
	done <- true

	/* Init the services */
	go updater(conn)
	go sender(conn)
	for {
		buf := make(packet, 1024)
		n, addr, err := conn.ReadFromUDP(buf)
		if err != nil {
			continue
		}
		go serveClient(conn, addr, buf[:n])
	}
}

func sender(conn *net.UDPConn) {
	slist := make(map[string]packet)
	for {
		select {
		case p := <-write_chan:
			k := p.Addr.String()
			v, ex := slist[k]
			if !ex {
				v = newPacket()
			}
			err := (&v).AddTLV(p.Tlv)
			if err != nil {
				_, err := conn.WriteToUDP(v, &p.Addr)
				if err != nil {
					wlog.Println(err)
				}
				ilog.Printf("Sent packet to %s", k)
				v = newPacket()
				(&v).AddTLV(p.Tlv)
			}
			slist[k] = v

		case <-time.After(100 * time.Millisecond):
			for k, v := range slist {
				addr, err := net.ResolveUDPAddr("udp", k)
				if err != nil {
					wlog.Println(err)
				}
				_, err = conn.WriteToUDP(v, addr)
				if err != nil {
					wlog.Println(err)
				}
				ilog.Printf("Sent packet to %s", k)
				delete(slist, k)
			}
		}
	}
}

func serveClient(conn *net.UDPConn, addr *net.UDPAddr, buf packet) {
	ilog.Printf("Received packet from %s", addr.String())

	/* Parsing packet */
	tlvs, err := buf.Parse()
	if err != nil {
		wlog.Printf("Invalid packet from %s : %s. abort", addr.String(), err)
		return
	}

	/* Tu sais parler ! On se connait ? */
	updateNeighbour(addr.String(), false)

	/* Treating tlvs */

	for _, t := range tlvs {
		ilog.Printf("TLV from %s : %s", addr.String(), TLVType(t.Type).String())
		switch t.Type {
		case NeigReq:
			neighlock.Lock()
			rand_addr := getRandNeighbour()
			if rand_addr.Addr.String() == addr.String() {
				ilog.Printf("Cant give neighbour")
				neighlock.Unlock()
				continue
			}
			neighlock.Unlock()
			tlv := TLV{Type: Neig, IP: rand_addr.Addr.IP, Port: uint16(rand_addr.Addr.Port)}
			addToQueue(*addr, tlv, write_chan)
			ilog.Printf("TLV to %s : Neighbour", rand_addr.Addr.String())

		case Neig:
			netlock.Lock()
			tlv := TLV{Type: NetHash, Hash: network.Hash()}
			netlock.Unlock()
			distAddr := net.UDPAddr{IP: t.IP, Port: int(t.Port)}
			addToQueue(distAddr, tlv, write_chan)
			ilog.Printf("TLV to %s : NetHash", distAddr.String())

		case NetHash:
			netlock.Lock()
			h1 := network.Hash()
			netlock.Unlock()
			if bytes.Equal(h1, t.Hash) {
				continue
			}
			tlv := TLV{Type: NetStateReq}
			addToQueue(*addr, tlv, write_chan)
			ilog.Printf("TLV to %s : Network State Request", addr.String())

		case NetStateReq:
			netlock.Lock()
			for _, v := range network {
				tlv :=
					TLV{Type: NodeHash, NodeID: v.Id, SeqNo: v.SeqNo,
						Hash: v.Hash()}
				addToQueue(*addr, tlv, write_chan)
				ilog.Printf("TLV to %s : Node Hash of %s", addr.String(), v.String())
			}
			netlock.Unlock()

		case NodeHash:
			ilog.Printf("Got Node Hash of %s", hex.EncodeToString(t.NodeID[:]))
			netlock.Lock()
			i, e := network[hex.EncodeToString(t.NodeID[:])]
			netlock.Unlock()
			if !e || !bytes.Equal(t.Hash, i.Hash()) {
				tlv := TLV{Type: NodeStateReq, NodeID: t.NodeID}
				addToQueue(*addr, tlv, write_chan)
				ilog.Printf("TLV to %s : Node State Request of %s", addr.String(), hex.EncodeToString(t.NodeID[:]))
			}

		case NodeStateReq:
			netlock.Lock()
			i, e := network[hex.EncodeToString(t.NodeID[:])]
			netlock.Unlock()
			if !e {
				continue
			}
			tlv := TLV{Type: NodeState, NodeID: i.Id, SeqNo: i.SeqNo, Hash: i.Hash(),
				Data: i.Data}
			addToQueue(*addr, tlv, write_chan)
			ilog.Printf("TLV to %s : Node State of %s", addr.String(), hex.EncodeToString(i.Id[:]))

		case NodeState:
			ilog.Printf("Got Node State of %s", hex.EncodeToString(t.NodeID[:]))
			netlock.Lock()
			localLock.Lock()
			if localNode.Id == t.NodeID {
				if !bytes.Equal(t.Hash, localNode.Hash()) && seqLt(localNode.SeqNo, t.SeqNo) {
					localNode.SeqNo = t.SeqNo + 1
					addToNet(localNode)
					ilog.Println("Increased sequence number of localnode")
				}
			} else {
				i, e := network[hex.EncodeToString(t.NodeID[:])]
				if !bytes.Equal(t.Hash, i.Hash()) && (!e || !seqLt(t.SeqNo, i.SeqNo)) {
					network[hex.EncodeToString(t.NodeID[:])] = Node{Id: t.NodeID, SeqNo: t.SeqNo, Data: t.Data}
					ilog.Printf("Added node %s version %d with message %s",
						hex.EncodeToString(t.NodeID[:]), t.SeqNo, t.Data)
				}
			}
			localLock.Unlock()
			netlock.Unlock()

		case Warning:
			wlog.Printf("Received Warning : %s", t.Data)
		}
	}
}

func updater(conn *net.UDPConn) {
	for {

		/* cleanup */
		neighlock.Lock()
		var rand_neigh net.UDPAddr
		for k, v := range neighbours {
			if !v.Permanent && time.Since(v.LastSeen) > 70*time.Second {
				ilog.Printf("Ditching peer %s", v.Addr.String())
				delete(neighbours, k)
			}
			rand_neigh = v.Addr
		}

		/* forever alone */
		if len(neighbours) <= 5 {
			tlv := TLV{Type: NeigReq}
			addToQueue(rand_neigh, tlv, write_chan)
			ilog.Printf("TLV to %s : Neighbour Request", rand_neigh.String())
		}
		neighlock.Unlock()

		/* send network tlv */
		netlock.Lock()
		tlv := TLV{Type: NetHash}
		tlv.Hash = network.Hash()
		netlock.Unlock()
		neighlock.Lock()
		for _, v := range neighbours {
			addToQueue(v.Addr, tlv, write_chan)
			ilog.Printf("TLV to %s : Network Hash", v.Addr.String())
		}
		neighlock.Unlock()

		time.Sleep(20 * time.Second)
	}
}
